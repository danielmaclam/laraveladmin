# {{config('app.name')}}
###Description
{{config('app.name')}} is a simple to use package allowing management 
of users roles and allowed actions.

###Dependencies
* PHP 7.0+
* Laravel 5.5.*
* Laravel Collective HTML ^5.4.0

###Prerequisites
You must be using Laravel's built in authentication. To set that up 
run the following commands

    php artisan make:auth
    php artisan migrate

###Installation
Add the git repository to your composer.json
       
       "repositories": [
            {
                "type": "git",
                "url": "git@bitbucket.org:danielmaclam/laraveladmin.git"
            }
        ],
Add the package name to the composer.json
        
        "require": {
            "dmaclam/laraveladmin": "dev-master"
        },
        
Run composer install
        
       composer install
Publish vendor files

        php artisan vendor:publish
        
   Make sure to select the dmaclam/laraveladmin service provider.
   
Run migrations

        php artisan migrate
        
Add the {{config('app.name')}} middleware to the app/http/kernel.php $routeMiddleware array.

       'laraveladmin' => \Dmaclam\LaravelAdmin\Middleware\LaravelAdminMiddleware::class,

Add the HasRoleInterface and HasActionInterface to the User model.
        
       class User extends Authenticatable implements HasRoleInterface, HasActionInterface
  
Add the HasRoleTrait and HasActionTrait to the user model.

       class User extends Authenticatable implements HasRoleInterface, HasActionInterface{
           use HasRoleTrait;
           use HasActionTrait;
       }
       
Lets have a look at {{config('app.name')}}
        
       Navigate to {site_base_url}/admin 
       
   If you have moved your models into another namespace you will 
   have to change the configuration in the config/laraveladmin.php
   file. (see Configuration)
   
###Configuration Options

#####Enable/Disable the {{config('app.name')}} package:
    
    'enabled' => 'true'
    
#####Control pagination on index pages:

This is the number of displayed rows per page.
    
    'models_per_page' => 10
    
#####Model Namespace:

This should be the same namespace as you User model. By default Laravel
places it in 'App\\'.

    'model_namespace' => 'App\\'
    
###Contributing

If you wish to contribute to this package please email daniel@danielmaclam.ca
for more information.

###Versioning

Available versions:

* dev-master - this is the most upto date code but may have bugs and
unknown behaviours.

###License

MIT License

Copyright (c) 2017 Daniel Maclam.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.