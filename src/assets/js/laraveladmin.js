/**
 * simple confirmation for deleting a single type of item on a page
 */
let initDeleteConfirm = function(item_type) {

    $('.delete-btn').on('click', function (e) {
        if (!confirm('Are you sure you want to delete this ' + item_type + '?')) {
            e.preventDefault();
        }
    });
};

let initCheckboxes = function(){

    $(':checkbox:checked').closest('.switch').addClass('is-checked');

    $('input[type="checkbox"]').on('click', function() {
        $(this).closest('.switch').toggleClass('is-checked');
    });
};