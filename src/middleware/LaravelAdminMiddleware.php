<?php

namespace Dmaclam\LaravelAdmin\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class LaravelAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            if(Auth::user()->hasRoles(['Admin', 'Root'], true)){
                return $next($request);
            }else{
                return abort(403,'You are not authorized to view this page.');
            }

        }else{
            return abort(401, 'You must be logged in to view this page.');
        }



    }
}
