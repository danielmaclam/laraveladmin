@if(Session::has('success'))
    <div class="col-md-10 col-md-offset-1">
        <div class="alert alert-success">
            <p>
                <strong>Success:</strong>
                {{Session::get('success')}}
            </p>
        </div>
    </div>
@endif