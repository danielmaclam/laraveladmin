@extends('laraveladmin::layouts.app')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel">
            <div class="panel-heading">
                {{config('app.name')}} - Dashboard - Create Role
            </div>

            <div class="panel-body">

                {{Form::open()}}

                    @include('laraveladmin::dashboard.role.forms.partial.form')


                    <div class="col-md-12 text-center">
                        {{Form::submit('Create',[
                            'class' => 'btn btn-default'
                        ])}}
                    </div>

                {{Form::close()}}
            </div>
        </div>
    </div>
@endsection