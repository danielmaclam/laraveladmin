<div class="form-horizontal">

    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        <label for="name" class="col-md-4 control-label">Name</label>

        <div class="col-md-6">
            {{Form::text('name', null, [
                'id' => 'name',
                'class' => 'form-control'
            ])}}

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
        <label for="description" class="col-md-4 control-label">Description</label>

        <div class="col-md-6">
            {{Form::textarea('description', null, [
                'id' => 'description',
                'class' => 'form-control'
            ])}}

            @if ($errors->has('description'))
                <span class="help-block">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('enabled') ? ' has-error' : '' }}">
        <label for="enabled" class="col-md-4 control-label">Enabled</label>

        <div class="col-md-6">
            <div class="switch">
                {{Form::checkbox('enabled', 1, null, [
                    'id' => 'enabled'
                ])}}
                <label for="enabled" class="check-label"></label>
            </div>

            @if ($errors->has('description'))
                <span class="help-block">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
        </div>
    </div>
</div>

@section('scripts')
    <script>
        initCheckboxes();
    </script>
@endsection