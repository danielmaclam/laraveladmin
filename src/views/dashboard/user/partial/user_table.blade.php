<div class="table-responsive">
    <table class="table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>E-mail</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @if($users->isEmpty())
            <tr>
                <td colspan="3">
                    It seems like all your friends have left. Maybe you should should go
                    outside and round some up...
                </td>
            </tr>
        @endif
        @foreach($users as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>
                    <a href="{{route('admin.users.edit_roles', ['userid' => $user->id])}}" class="btn btn-default">Manage Roles</a>
                    <a href="{{route('admin.users.edit_actions', ['userid' => $user->id])}}" class="btn btn-default">Manage Actions</a>
                    <a href="{{route('admin.users.edit', ['userid' => $user->id])}}" class="btn btn-primary">Edit</a>
                    <a href="{{route('admin.users.delete', ['userid' => $user->id])}}" class="btn btn-danger delete-btn">Delete</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
