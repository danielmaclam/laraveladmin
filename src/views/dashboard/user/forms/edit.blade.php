@extends('laraveladmin::layouts.app')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel">
            <div class="panel-heading">
                {{config('app.name')}} - Dashboard - Edit User - {{$user->name}}
            </div>

            <div class="panel-body">

                {{Form::model($user)}}

                    @include('laraveladmin::dashboard.user.forms.partial.form')


                    <div class="col-md-12 text-center">
                        {{Form::submit('Update',[
                            'class' => 'btn btn-default'
                        ])}}
                    </div>

                {{Form::close()}}
            </div>
        </div>
    </div>
@endsection