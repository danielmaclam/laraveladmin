@extends('laraveladmin::layouts.app')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel">
            <div class="panel-heading">
                {{config('app.name')}} - Dashboard - Edit User - Manage Actions - {{$user->name}}
            </div>

            <div class="panel-body">

                {{Form::model($user->actions)}}

                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Role Name</th>
                                    <th>Description</th>
                                    <th>Enabled</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($availableActions as $action)
                                    <tr>
                                        <td>{{$action->name}}</td>
                                        <td>{{$action->description}}</td>
                                        <td>
                                            <div class="switch">
                                                {{Form::checkbox('actions[]', $action->id, $user->hasAction($action->name), [
                                                    'id' => 'action_' . $action->id
                                                ])}}
                                                <label for="action_{{$action->id}}" class="check-label"></label>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col-md-12 text-center">
                    {{Form::submit('Update',[
                        'class' => 'btn btn-default'
                    ])}}
                </div>

                {{Form::close()}}
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script>
        initCheckboxes();
    </script>
@endsection