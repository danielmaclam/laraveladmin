@extends('laraveladmin::layouts.app')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel">
            <div class="panel-heading">
                {{config('app.name')}} - Dashboard - Edit User - Manage Roles - {{$user->name}}
            </div>

            <div class="panel-body">

                {{Form::model($user->roles)}}

                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Role Name</th>
                                    <th>Description</th>
                                    <th>Enabled</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($availableRoles as $role)
                                    <tr>
                                        <td>{{$role->name}}</td>
                                        <td>{{$role->description}}</td>
                                        <td>
                                            <div class="switch">
                                                {{Form::checkbox('roles[]', $role->id, $user->hasRole($role->name), [
                                                    'id' => 'role_' . $role->id
                                                ])}}
                                                <label for="role_{{$role->id}}" class="check-label"></label>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col-md-12 text-center">
                    {{Form::submit('Update',[
                        'class' => 'btn btn-default'
                    ])}}
                </div>

                {{Form::close()}}
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script>
        initCheckboxes();
    </script>
@endsection