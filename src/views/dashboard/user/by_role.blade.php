@extends('laraveladmin::layouts.app')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel">
            <div class="panel-heading">
                {{config('app.name')}} - Dashboard - Users - By Role - {{$role->name}}
            </div>

            <div class="panel-body">
                @include('laraveladmin::dashboard.user.partial.user_table')

                <div class="col-md-12 text-center">
                    {{$users->render()}}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        initDeleteConfirm('user');
    </script>
@endsection