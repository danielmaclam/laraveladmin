@extends('laraveladmin::layouts.app')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel">
            <div class="panel-heading">
                {{config('app.name')}} - Dashboard - User Actions
            </div>

            <div class="panel-body">
                <div class="col-md-12">
                    <a href="{{route('admin.actions.create')}}" class="pull-right btn btn-default">Add User Action</a>
                </div>
                <div class="col-md-12 table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Description</th>
                                <th class="text-center">Enabled</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($actions->isEmpty())
                                <tr>
                                    <td colspan="5" class="text-center">
                                        <p>
                                            It seems like you don't have any actions configured.
                                        </p>

                                        <div>
                                            <a href="{{route('admin.actions.create')}}" class="btn btn-default">
                                                Add User Action
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endif
                            @foreach($actions as $action)
                                <tr class="text-center">
                                    <td class="text-left">{{$action->id}}</td>
                                    <td>{{$action->name}}</td>
                                    <td>{{$action->description}}</td>
                                    <td>
                                        @if($action->enabled)
                                            <span class="glyphicon glyphicon-ok"></span>
                                        @else
                                            <span class="glyphicon glyphicon-remove"></span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{route('admin.users.action', ['id' => $action->id])}}" class="btn btn-default">View Users</a>
                                        <a href="{{route('admin.actions.edit', ['id' => $action->id])}}" class="btn btn-primary">Edit</a>
                                        <a href="{{route('admin.actions.delete', ['id' => $action->id])}}" class="btn btn-danger delete-btn">Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        initDeleteConfirm('action');
    </script>
@endsection