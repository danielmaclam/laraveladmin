@extends('laraveladmin::layouts.app')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel">
            <div class="panel-heading">
                {{config('app.name')}} - Dashboard
            </div>

            <div class="panel-body">

                <div class="col-md-3">
                    <a href="{{route('admin.users.index')}}">
                        <div class="list-group-item">
                            <div class="pull-right glyphicon glyphicon-user"></div>
                            Users
                        </div>
                    </a>
                </div>

                <div class="col-md-3">
                    <a href="{{route('admin.roles.index')}}">
                        <div class="list-group-item">
                            <div class="pull-right glyphicon glyphicon-wrench"></div>
                            User Roles
                        </div>
                    </a>
                </div>

                <div class="col-md-3">
                    <a href="{{route('admin.actions.index')}}">
                        <div class="list-group-item">
                            <div class="pull-right glyphicon glyphicon-flash"></div>
                            User Actions
                        </div>
                    </a>
                </div>

            </div>
        </div>
    </div>
@endsection
