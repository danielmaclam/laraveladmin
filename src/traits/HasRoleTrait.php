<?php
/**
 * Created by Daniel Maclam.
 * Date: 24/11/17
 * Time: 8:39 PM
 */

namespace Dmaclam\LaravelAdmin\Traits;

use Dmaclam\LaravelAdmin\Models\Role;

trait HasRoleTrait
{
    public function roles(){
        return $this->belongsToMany('\Dmaclam\LaravelAdmin\Models\Role');
    }

    public function hasRole($role){
        if(is_array($role)){
            return $this->hasRoles($role);
        }

        return $this->roles->where('name', $role)->count() === 1;
    }

    public function hasRoles(Array $roles, bool $any = false){
        $count = $this->roles->whereIn('name', $roles)->count();

        if($any){
            return $count >= 1;
        }else{
            return $count === count($roles);
        }
    }

    public function addRole($role){
        if(is_array($role)){
            $this->addRoles($role);
            return;
        }

        $role = Role::where('name', $role)->first();

        if(!$role){
            throw new \Exception('RoleNotFoundException');
        }

        $this->roles()->syncWithoutDetaching($role->id);
    }

    public function addRoles(Array $roles){
        $role = Role::whereIn('name', $roles)->pluck('id');

        if(count($roles) != $role->count()){
            throw new \Exception('RoleNotFoundException');
        }

        $this->roles()->syncWithoutDetaching($role->toArray());
    }

    public function removeRole($role){
        if(is_array($role)){
            $this->removeRoles($role);
            return;
        }

        $role = Role::where('name', $role)->first();

        if(!$role){
            throw new \Exception('RoleNotFoundException');
        }

        $this->roles()->detach($role->id);
    }

    public function removeRoles(Array $roles){
        $role = Role::whereIn('name', $roles)->pluck('id');

        if(count($roles) != $role->count()){
            throw new \Exception('RoleNotFoundException');
        }

        $this->roles()->detach($role->toArray());
    }
}