<?php

namespace Dmaclam\LaravelAdmin\Traits;

use Dmaclam\LaravelAdmin\Models\Action;

trait HasActionTrait
{
    public function actions(){
        return $this->belongsToMany('\Dmaclam\LaravelAdmin\Models\Action');
    }

    public function hasAction($action){
        if(is_array($action)){
            return $this->hasActions($action);
        }

        return $this->actions->where('name', $action)->count() === 1;
    }

    public function hasActions(Array $actions, bool $any = false){
        $count = $this->actions->whereIn('name', $actions)->count();

        if($any){
            return $count >= 1;
        }else{
            return $count === count($actions);
        }
    }

    public function addAction($action){
        if(is_array($action)){
            $this->addActions($action);
            return;
        }

        $action = Action::where('name', $action)->first();

        if(!$action){
            throw new \Exception('ActionNotFoundException');
        }

        $this->actions()->syncWithoutDetaching($action->id);
    }

    public function addActions(Array $actions){
        $action = Action::whereIn('name', $actions)->pluck('id');

        if(count($actions) != $action->count()){
            throw new \Exception('ActionNotFoundException');
        }

        $this->actions()->syncWithoutDetaching($action->toArray());
    }

    public function removeAction($action){
        if(is_array($action)){
            $this->removeActions($action);
            return;
        }

        $action = Action::where('name', $action)->first();

        if(!$action){
            throw new \Exception('ActionNotFoundException');
        }

        $this->actions()->detach($action->id);
    }

    public function removeActions(Array $actions){
        $action = Role::whereIn('name', $actions)->pluck('id');

        if(count($actions) != $action->count()){
            throw new \Exception('ActionNotFoundException');
        }

        $this->roles()->detach($action->toArray());
    }
}