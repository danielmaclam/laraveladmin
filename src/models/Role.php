<?php

namespace Dmaclam\LaravelAdmin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'enabled'
    ];

    public function users(){
        return $this->belongsToMany(config('laraveladmin.model_namespace','App\\') . 'User');
    }
}
