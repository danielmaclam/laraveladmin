<?php

namespace Dmaclam\LaravelAdmin\Requests;

use Illuminate\Validation\Rule;
use Dmaclam\LaravelAdmin\Models\Role;
use Illuminate\Foundation\Http\FormRequest;

class LaravelAdminRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //check if we trashed an role if so we will just restore it
        $trashed = Role::onlyTrashed()->where('name', $this->get('name'))->first();

        if($trashed){
            $uniqueRule = Rule::unique('actions', 'name')->ignore($trashed->id);
        }else{
            $uniqueRule = '';
        }

        return [
            'name' => 'required|'. $uniqueRule,
        ];
    }
}
