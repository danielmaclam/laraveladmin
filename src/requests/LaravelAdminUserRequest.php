<?php

namespace Dmaclam\LaravelAdmin\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class LaravelAdminUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|' . Rule::unique('users')->ignore($this->id),
            'address_1' => 'required',
            'city' => 'required',
            'province_id' => 'required',
            'postal' => 'required'
        ];
    }
}
