<?php

namespace Dmaclam\LaravelAdmin\Controllers;

use App\Http\Controllers\Controller;
class LaravelAdminController extends Controller
{
    public function index(){

        return view('laraveladmin::dashboard.index');
    }
}
