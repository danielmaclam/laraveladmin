<?php

namespace Dmaclam\LaravelAdmin\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Dmaclam\LaravelAdmin\Models\Role;
use Dmaclam\LaravelAdmin\Requests\LaravelAdminRoleRequest;

class LaravelAdminRoleController extends Controller
{
    public function index(){
        $roles = Role::paginate(config('laraveladmin.models_per_page', 10));

        return view('laraveladmin::dashboard.role.index',[
            'roles' => $roles,
        ]);
    }

    public function create(){
        return view('laraveladmin::dashboard.role.forms.create');
    }

    public function store(LaravelAdminRoleRequest $request){
        //check if that role was ever soft deleted
        $role = Role::where('name',$request->get('name'))->onlyTrashed()->first();

        if($role){
            $role->update($request->all());
            $role->restore();
        }else{
            $role = Role::create($request->all());
        }

        $request->session()->flash('success', 'The role ' . $role->name . ' has been added.');

        return redirect()->route('admin.roles.index');
    }

    public function edit($id){
        $role = Role::findOrFail($id);

        return view('laraveladmin::dashboard.role.forms.edit',[
            'role' => $role,
        ]);
    }

    public function update(LaravelAdminRoleRequest $request, $id){
        if(!$request->get('enabled')){
            $request->request->add(['enabled' => 0]);
        }

        $role = Role::findOrFail($id);
        $role->update($request->all());

        $request->session()->flash('success', 'The role ' . $role->name . ' has been updated.');

        return redirect()->route('admin.roles.index');
    }

    public function delete(Request $request, $id){
        $role = Role::findOrFail($id);
        $name = $role->name;
        $role->delete();

        $request->session()->flash('success', 'The role ' . $name . ' has been deleted.');

        return redirect()->route('admin.roles.index');
    }
}
