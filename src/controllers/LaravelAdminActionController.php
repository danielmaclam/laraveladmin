<?php

namespace Dmaclam\LaravelAdmin\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Dmaclam\LaravelAdmin\Models\Action;
use Dmaclam\LaravelAdmin\Requests\LaravelAdminActionRequest;

class LaravelAdminActionController extends Controller
{
    public function index(){
        $actions = Action::paginate(config('laraveladmin.models_per_page', 10));

        return view('laraveladmin::dashboard.action.index',[
            'actions' => $actions,
        ]);
    }

    public function create(){
        return view('laraveladmin::dashboard.action.forms.create');
    }

    public function store(LaravelAdminActionRequest $request){
        //check if that role was ever soft deleted
        $action = Action::where('name',$request->get('name'))->onlyTrashed()->first();

        if($action){
            $action->update($request->all());
            $action->restore();
        }else{
            $action = Action::create($request->all());
        }

        $request->session()->flash('success', 'The action ' . $action->name . ' has been added.');


        return redirect()->route('admin.actions.index');
    }

    public function edit($id){
        $action = Action::findOrFail($id);

        return view('laraveladmin::dashboard.action.forms.edit',[
            'action' => $action,
        ]);
    }

    public function update(LaravelAdminActionRequest $request, $id){
        $action = Action::findOrFail($id);

        if(!$request->get('enabled')){
            $request->request->add(['enabled' => 0]);
        }

        $action->update($request->all());

        $request->session()->flash('success', 'The action ' . $action->name . ' has been updated.');

        return redirect()->route('admin.actions.index');
    }

    public function delete(Request $request, $id){
        $action = Action::findOrFail($id);
        $name = $action->name;
        $action->delete();

        $request->session()->flash('success', 'The action ' . $name . ' has been deleted.');

        return redirect()->route('admin.actions.index');
    }
}
