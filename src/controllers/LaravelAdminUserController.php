<?php

namespace Dmaclam\LaravelAdmin\Controllers;

use App\Address;
use App\Country;
use App\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Dmaclam\LaravelAdmin\Models\Role;
use Dmaclam\LaravelAdmin\Models\Action;
use Dmaclam\LaravelAdmin\Requests\LaravelAdminUserRequest;
use Illuminate\Support\Facades\Input;

class LaravelAdminUserController extends Controller
{
    protected $user;

    public function __construct()
    {
        $user_model_fqn = (config('laraveladmin.model_namespace', 'App\\') . 'User');
        $this->user = new $user_model_fqn;
    }

    public function index(){
        $users = $this->user->where('id', '<>', Auth::check() ? Auth::user()->id : 0)
            ->with('roles')
            ->paginate(config('laraveladmin.models_per_page', 10));

        return view('laraveladmin::dashboard.user.index',[
            'users' => $users,
            'searchRoles' => $this->getSearchTerms()
        ]);
    }

    public function userByRole($roleId){
        $role = Role::findOrFail($roleId);
        $users = $role->users()
            ->where('users.id', '<>', Auth::check() ? Auth::user()->id : 0)
            ->with('roles')
            ->paginate(config('laraveladmin.models_per_page', 10));

        return view('laraveladmin::dashboard.user.by_role',[
            'role' => $role,
            'users' => $users,
            'searchRoles' => $this->getSearchTerms()
        ]);
    }

    public function userByAction($actionId){
        $action = Action::findOrFail($actionId);
        $users = $action->users()
            ->where('users.id', '<>', Auth::check() ? Auth::user()->id : 0)
            ->with('roles')
            ->paginate(config('laraveladmin.models_per_page', 10));

        return view('laraveladmin::dashboard.user.by_action',[
            'action' => $action,
            'users' => $users,
            'searchRoles' => $this->getSearchTerms()
        ]);
    }

    public function edit($userid){
        $user = $this->user->findOrFail($userid);

        if($user->address){
            $provinces = Province::where('country_id', $user->address->country_id)->get();
        }else{
            $provinces =  Province::where('country_id', 1)->get();
        }
        $countries = Country::all();

        return view('laraveladmin::dashboard.user.forms.edit',[
            'user' => $user,
            'address' => $user->address ? $user->address : new Address(),
            'provinces' => $provinces->pluck('name', 'id'),
            'countries' => $countries->pluck('name', 'id'),
        ]);
    }

    public function update(LaravelAdminUserRequest $request, $userid){
        $user = $this->user->findOrFail($userid);

        $address = $user->address ? $user->address: new Address();
        $address->fill($request->all());
        $address->user_id = $user->id;
        $address->save();

        $user->update($request->all());

        $user->fresh();

        $request->session()->flash('success', 'The user ' . $user->name . ' has been updated.');

        $users = $this->user->where('id', '<>', Auth::check() ? Auth::user()->id : 0)
            ->with('roles')
            ->paginate(config('laraveladmin.models_per_page', 10));

        return redirect()->route('admin',[
            'users' => $users,
            'searchRoles' => $this->getSearchTerms()
        ]);
    }

    public function editRoles($id){
        $user = $this->user->findOrFail($id);
        $availableRoles = Role::where('enabled', 1)->get();

        return view('laraveladmin::dashboard.user.forms.manage_roles',[
            'user' => $user,
            'availableRoles' => $availableRoles,
        ]);
    }

    public function updateRoles($id, Request $request){

        $roles = $request->get('roles');

        $ids = [];
        if(is_array($roles)) {
            $ids = array_values($roles);
        }

        $user = $this->user->findOrFail($id);
        $user->roles()->sync($ids);
        $user->fresh();

        $availableRoles = Role::where('enabled', 1)->get();

        $request->session()->flash('success', 'The user roles for ' . $user->name . ' have been updated.');
        $request->session()->flash('referer', $request->get('referer'));

        return redirect()->route('admin.users.edit_roles',[
            'user' => $user,
            'availableRoles' => $availableRoles,
        ]);
    }

    public function editActions($id){
        $user = $this->user->findOrFail($id);
        $availableActions = Action::where('enabled', 1)->get();

        return view('laraveladmin::dashboard.user.forms.manage_actions',[
            'user' => $user,
            'availableActions' => $availableActions,
        ]);
    }

    public function updateActions($id, Request $request){
        $action = $request->get('actions');

        $ids = [];
        if(is_array($action)) {
            $ids = array_values($action);
        }

        $user = $this->user->findOrFail($id);
        $user->actions()->sync($ids);
        $user->fresh();

        $availableActions = Action::where('enabled', 1)->get();

        $request->session()->flash('success', 'The user actions for ' . $user->name . ' have been updated.');
        $request->session()->flash('referer', $request->get('referer'));

        return redirect()->route('admin.users.edit_actions',[
            'user' => $user,
            'availableActions' => $availableActions,
        ]);
    }

    public function delete($id, Request $request){
        $user = $this->user->findOrFail($id);
        $name = $user->name;

        $user->roles()->sync([]);
        $user->actions()->sync([]);
        $user->delete();

        $request->session()->flash('success', 'The user ' . $user->name . ' has been deleted.');

        $users = $this->user->where('id', '<>', Auth::check() ? Auth::user()->id : 0)
            ->with('roles')
            ->paginate(config('laraveladmin.models_per_page', 10));

        return redirect()->route('admin.users.index',[
            'users' => $users,
        ]);
    }

    public function search(Request $request){
        $searchTerm = $request->get('search_term');
        $searchRole = $request->get('search_role');
        $phoneNumber = $request->get('phone_number');

        if($searchRole === 'All'){
            $users = $this->user
                ->where('id', '<>', Auth::check() ? Auth::user()->id : 0)
                ->where(function($query) use ($searchTerm){
                    $query->where('name', 'like', "%$searchTerm%")
                        ->orWhere('email', 'like', "%$searchTerm%");
                })
                ->where('phone_number','like',  '%' . $phoneNumber . '%')
                ->with('roles')
                ->paginate(config('laraveladmin.models_per_page', 10));
        }else{
            $users = Role::where('name', $searchRole)->first()->users()
                ->where('users.id', '<>', Auth::check() ? Auth::user()->id : 0)
                ->where(function($query) use ($searchTerm) {
                    $query->where('name', 'like', "%$searchTerm%")
                        ->orWhere('email', 'like', "%$searchTerm%");
                })
                ->where('phone_number','like',  '%' . $phoneNumber . '%')
                ->with('roles')
                ->paginate(config('laraveladmin.models_per_page', 10));
        }

        $users->appends(Input::all());

        return view('laraveladmin::dashboard.user.index',[
            'users' => $users,
            'searchRoles' => $this->getSearchTerms()
        ]);
    }

    private function getSearchTerms(){
        return Role::all()
            ->filter(function($item){
                return $item->name !== 'Root';
            })
            ->pluck('name','name')
            ->prepend('All', 'All');
    }
}

