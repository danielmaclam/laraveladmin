<?php

namespace Dmaclam\LaravelAdmin\Interfaces;

interface HasRoleInterface
{
    public function roles();
    public function hasRole($role);
    public function hasRoles(Array $roles, bool $any = false);
    public function addRole($role);
    public function addRoles(Array $roles);
    public function removeRole($role);
    public function removeRoles(Array $roles);
}