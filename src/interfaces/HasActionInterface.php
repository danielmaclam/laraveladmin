<?php

namespace Dmaclam\LaravelAdmin\Interfaces;

interface HasActionInterface
{
    public function actions();
    public function hasAction($action);
    public function hasActions(Array $actions, bool $any = false);
    public function addAction($action);
    public function addActions(Array $actions);
    public function removeAction($action);
    public function removeActions(Array $actions);
}