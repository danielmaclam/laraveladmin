<?php

namespace Dmaclam\LaravelAdmin;

use Illuminate\Support\ServiceProvider;

class LaravelAdminServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/configs/config.php' => config_path('laraveladmin.php'),
        ]);

        $this->loadMigrationsFrom(__DIR__.'/migrations');

        $this->loadRoutesFrom(__DIR__.'/routes/routes.php');

        $this->publishes([
            __DIR__.'/assets/css' => public_path('laraveladmin/css'),
        ], 'public');

        $this->publishes([
            __DIR__.'/assets/js' => public_path('laraveladmin/js'),
        ], 'public');

        $this->loadViewsFrom(__DIR__.'/views', 'laraveladmin');

        $this->publishes([
            __DIR__.'/views' => base_path('resources/views/dmaclam/laraveladmin'),
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
