<?php

if(config('laraveladmin.enabled', false)){
    Route::group([
        'namespace' => 'Dmaclam\\LaravelAdmin\\Controllers',
        'middleware' => [
            'web',
            'laraveladmin'
        ],
        'as' => 'admin',
        'prefix' => 'admin',
    ], function(){
        Route::get('/', 'LaravelAdminController@index');

        Route::group([
            'as' => '.users.',
            'prefix' => 'users',
        ],function(){
            Route::get('/', 'LaravelAdminUserController@index')->name('index');
            Route::get('/role/{roleid}', 'LaravelAdminUserController@userByRole')->name('role');
            Route::get('/action/{actionid}', 'LaravelAdminUserController@userByAction')->name('action');
            Route::get('{id}/edit', 'LaravelAdminUserController@edit')->name('edit');
            Route::post('{id}/edit', 'LaravelAdminUserController@update')->name('update');
            Route::get('{id}/roles/edit', 'LaravelAdminUserController@editRoles')->name('edit_roles');
            Route::post('{id}/roles/edit', 'LaravelAdminUserController@updateRoles')->name('update_roles');
            Route::get('{id}/actions/edit', 'LaravelAdminUserController@editActions')->name('edit_actions');
            Route::post('{id}/actions/edit', 'LaravelAdminUserController@updateActions')->name('update_actions');
            Route::get('{id}/delete', 'LaravelAdminUserController@delete')->name('delete');
            Route::get('/search', 'LaravelAdminUserController@search')->name('search');
        });

        Route::group([
            'as' => '.roles.',
            'prefix' => 'roles',
        ],function(){
            Route::get('/', 'LaravelAdminRoleController@index')->name('index');
            Route::get('/create', 'LaravelAdminRoleController@create')->name('create');
            Route::post('/create', 'LaravelAdminRoleController@store')->name('store');
            Route::get('/edit/{id}', 'LaravelAdminRoleController@edit')->name('edit');
            Route::post('/edit/{id}', 'LaravelAdminRoleController@update')->name('update');
            Route::get('/delete/{id}', 'LaravelAdminRoleController@delete')->name('delete');
        });

        Route::group([
            'as' => '.actions.',
            'prefix' => 'actions',
        ],function(){
            Route::get('/', 'LaravelAdminActionController@index')->name('index');
            Route::get('/create', 'LaravelAdminActionController@create')->name('create');
            Route::post('/create', 'LaravelAdminActionController@store')->name('store');
            Route::get('/edit/{id}', 'LaravelAdminActionController@edit')->name('edit');
            Route::post('/edit/{id}', 'LaravelAdminActionController@update')->name('update');
            Route::get('/delete/{id}', 'LaravelAdminActionController@delete')->name('delete');
        });

    });
}

