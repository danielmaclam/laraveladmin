<?php

return [

    /**
     * Enable / Disable the {{config('app.name')}} Package
     */
    'enabled' => 'true',

    /**
     * set the models displayed per page
     */
    'models_per_page' => 10,

    /**
     * set the model namespace
     * if you have moved your models
     * into another namespace provide it here
     */
    'model_namespace' => 'App\\',

    /**
     * Default roles for new users
     */
    'default_roles' => [

    ],

    /**
     * Default actions for new users
     */
    'default_actions' => [

    ],
];